##############
IMAP Forwarder
##############

Fetches / monitors a source (IMAP) mail box, and transfers existing / new messages to a destination (IMAP) mailbox.

Because we know you were never really fond of entrusting the source e-mail provider with the destination's credentials.

Getting Started
###############

Prerequisites
*************

- IMAP client library:

.. code:: sh

   pip3 install imapclient


Usage
#####

The credential data is passed via environment variables so they are not visible
to other users on the system.

Mandatory Configuration
***********************

.. list-table::
   :widths: 27 8 70
   :header-rows: 1

   * - ARGUMENT
     - Type
     - Description
   * - IMAPFWD_SRC_HOST
     - string
     - IMAP hostname of source folder to connect to.
   * - IMAPFWD_SRC_LOGIN
     - string
     - Username of source folder to authenticate as.
   * - IMAPFWD_SRC_PASSWORD
     - string
     - Password to be used with username of source folder.
   * - IMAPFWD_SRC_FOLDER (typically INBOX).
     - string
     - Folder to be fetched / monitored.
   * - IMAPFWD_DST_HOST
     - string
     - IMAP hostname of destination folder to connect to.
   * - IMAPFWD_DST_LOGIN
     - string
     - Username of destination folder to authenticate as.
   * - IMAPFWD_DST_PASSWORD
     - string
     - Password to be used with username of destination folder.
   * - IMAPFWD_DST_FOLDER
     - string
     - Destination folder (typically INBOX).


Optional Configuration
**********************

.. list-table::
   :widths: 27 8 70
   :header-rows: 0

   * - IMAPFWD_IDLE_CHECK_TIMEOUT
     - real
     - Server idle check timeout.



Usage template
**************

The following shell prototype can be used when in the local repository folder:

.. code:: sh

   export IMAPFWD_SRC_HOST=... \
    IMAPFWD_SRC_LOGIN=... \
    IMAPFWD_SRC_PASSWORD=... \
    IMAPFWD_SRC_FOLDER=... \
    IMAPFWD_DST_HOST=... \
    IMAPFWD_DST_LOGIN=... \
    IMAPFWD_DST_PASSWORD=... \
    IMAPFWD_DST_FOLDER=...

   python -m imapfwd


License
#######

Distributed under the MIT License. See ``LICENSE`` for more information.

