#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Forward IMAP messages from one mailbox/folder to another mailbox/folder

import sys, os
import email, time, traceback, logging

import imapclient

log = logging.getLogger()

class MailHandler:

	def __init__(self, src, dst):
		self.src = src
		self.dst = dst
		self.connection_refresh()

		if not self.dst_imap_client.folder_exists(self.dst.folder):
			self.dst_imap_client.create_folder(self.dst.folder)
			log.info('Folder %s created at %s@%s' %(self.dst.folder,
			 self.dst.username,
			 self.dst.host
			))

	def connect(self, host, ssl):
		log.info('connecting to IMAP server - %s' %(host))
		imap_client = imapclient.IMAPClient(host, use_uid=True, ssl=ssl)
		log.info('server connection established')
		return imap_client

	def login(self, username, password, imap_client):
		log.info('logging in to IMAP server - {0}'.format(username))
		result = imap_client.login(username, password)
		log.info('login successful - {0}'.format(result))
		return result

	def select_folder(self, folder, imap_client):
		log.info('selecting IMAP folder - {0}'.format(folder))
		result = imap_client.select_folder(folder)
		log.info('folder selected')
		return result

	def fwd_unseen(self):
		msgs = self.src_imap_client.search('UNSEEN')
		log.info('{0} unread messages seen - {1}'.format(
		 len(msgs), msgs
		))

		for msg in msgs:
			# Fetch unseen msg from source
			result = self.src_imap_client.fetch(msg, ['BODY.PEEK[]'])
			log.debug('Fecth result: %s' %(result))
			mail = result[msg][b'BODY[]']

			# Append to dst folder
			self.dst_imap_client.append(self.dst.folder, mail)
			self.src_imap_client.add_flags(msg, (imapclient.SEEN,))
			processed = email.message_from_bytes(mail)
			log.info('FWD email ID %s, From: %s Date: %s Subject: %s' %(
			 msg,
			 processed['From'],
			 processed['date'],
			 processed['Subject'],
			))

	def connection_refresh(self):
		self.src_imap_client = self.connect(self.src.host, self.src.ssl)
		_ = self.login(self.src.username, self.src.password, self.src_imap_client)
		_ = self.select_folder(self.src.folder, self.src_imap_client)

		self.dst_imap_client = self.connect(self.dst.host, self.dst.ssl)
		_ = self.login(self.dst.username, self.dst.password, self.dst_imap_client)

	def monitor(self, idle_check_timeout):
		self.fwd_unseen()

		while True:
			log.info('Monitoring... Timeout of %f second(s), src:%s at %s@%s > dst:%s at %s@%s' %(
			 idle_check_timeout,
			 self.src.folder,
			 self.src.username,
			 self.src.host,
			 self.dst.folder,
			 self.dst.username,
			 self.dst.host,
			))
			self.src_imap_client.idle()
			result = self.src_imap_client.idle_check(idle_check_timeout)
			if result:
				self.src_imap_client.idle_done()
				self.fwd_unseen()
			else:
				self.src_imap_client.idle_done()
				self.src_imap_client.noop()
				log.info('No new messages seen')

		self.connection_refresh() #TODO: manage refresh properly

class MailLocationInfo:

	def __init__(self,
	 host,
	 username,
	 password,
	 ssl=True,
	 folder='INBOX',
	 download='.',
	 ):
		self.host = host
		self.username = username
		self.password = password
		self.ssl = ssl
		self.folder = folder
		self.download = download


def main():

	import argparse

	parser = argparse.ArgumentParser(
	 description="Forward IMAP messages from one mailbox/folder to another mailbox/folder",
	)

	parser.add_argument("--log-level",
	 default="INFO",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	try:
		import argcomplete
		argcomplete.autocomplete(parser)
	except:
		pass

	args = parser.parse_args()

	logging.basicConfig(
	 level=getattr(logging, args.log_level),
	 format="%(levelname)s %(message)s"
	)

	src = MailLocationInfo(os.environ['IMAPFWD_SRC_HOST'],
	 os.environ['IMAPFWD_SRC_LOGIN'],
	 os.environ['IMAPFWD_SRC_PASSWORD'],
	 folder=os.environ['IMAPFWD_SRC_FOLDER'],
	)

	dst = MailLocationInfo(os.environ['IMAPFWD_DST_HOST'],
	 os.environ['IMAPFWD_DST_LOGIN'],
	 os.environ['IMAPFWD_DST_PASSWORD'],
	 folder=os.environ['IMAPFWD_DST_FOLDER'],
	)

	mail_hander = MailHandler(src, dst)

	idle_check_timeout = float(os.environ.get('IMAPFWD_IDLE_CHECK_TIMEOUT', 5*60))
	mail_hander.monitor(idle_check_timeout)

	return 0


if __name__ == '__main__':

	res = main()
	raise SystemExit(res)
